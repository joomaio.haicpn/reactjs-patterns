import {createStore} from 'redux';
import reducer from '../reducers/reducer';

//Store 

const initialState = {
    name: "Cao Pham Ngoc Hai",
    description: "Student at Hanoi University of Science and Technology, Fresher Frontend Developer, Footballer and Guitar Player for joys",
    tech: "Bootstrap 4, Javascript - ReactJS and NodeJS",
    location: "Hanoi"
};

const store = createStore(reducer, initialState);

export default store;